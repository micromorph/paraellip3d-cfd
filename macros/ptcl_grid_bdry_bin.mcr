#!MC 1410
$!FieldLayers ShowMesh = Yes
$!FieldLayers ShowShade = Yes
$!FieldLayers ShowEdge = Yes

$!FieldMap [1]  Mesh{LineThickness = 0.3}
$!FieldMap [2]  Mesh{LineThickness = 0.3}
$!FieldMap [2]  Mesh{Color = Green}
$!FieldMap [1]  Shade{Show = No}
$!FieldMap [2]  Shade{Show = No}
$!FieldMap [1]  Surfaces{SurfacesToPlot = ExposedCellFaces}
$!FieldMap [2]  Surfaces{SurfacesToPlot = ExposedCellFaces}
$!FieldMap [3]  Mesh{Show = No}
$!FieldMap [3]  Shade{Color = Custom11}

