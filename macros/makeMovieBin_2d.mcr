#!MC 1410
$!VarSet |MFBD| = ''
$!Varset |NAME| = "2d"
$!OpenLayout  "|NAME|.lay"

$!ExportSetup ExportFormat = PNG
$!ExportSetup ImageWidth = 3600
$!ExportSetup ExportFName = "|NAME|_ini.png"
$!Export 
  ExportRegion = CurrentFrame

$!ExportSetup ExportFormat = MPEG4
$!ExportSetup ImageWidth = 3600
$!ExportSetup ExportFName = "|NAME|.mp4"
$!AnimateTime 
  StartTime = 0
  EndTime = 99
  Skip = 1
  CreateMovieFile = Yes

$!ExportSetup ExportFormat = PNG
$!ExportSetup ImageWidth = 3600
$!ExportSetup ExportFName = "|NAME|_end.png"
$!Export 
  ExportRegion = CurrentFrame

$!RemoveVar |MFBD|
