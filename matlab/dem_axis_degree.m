function dem_axis_degree(fileToRead1)
%userpath('/home/yanb/matlab/')
screenWidth = 1.0;

fIn = fopen(fileToRead1);
row = fscanf(fIn, '%d', 1);
col = 29;
text = fscanf(fIn, '%s', col);
numeric = zeros(row, col);
for i = 1:row
	for j = 1:col
  numeric(i, j) = fscanf(fIn, '%e', 1);
 end
end
fclose(fIn);

% angle w.r.t axis, x denotes axis x
axis_a_x = numeric(:, 9);
axis_a_y = numeric(:, 10);
axis_a_z = numeric(:, 11);
axis_b_x = numeric(:, 12);
axis_b_y = numeric(:, 13);
axis_b_z = numeric(:, 14);
axis_c_x = numeric(:, 15);
axis_c_y = numeric(:, 16);
axis_c_z = numeric(:, 17);

% angle w.r.t plane, x denotes x plane (i.e., yz plane)
axis_a_x = 90 - axis_a_x * (180.0/pi);
axis_a_y = 90 - axis_a_y * (180.0/pi);
axis_a_z = 90 - axis_a_z * (180.0/pi);
axis_b_x = 90 - axis_b_x * (180.0/pi);
axis_b_y = 90 - axis_b_y * (180.0/pi);
axis_b_z = 90 - axis_b_z * (180.0/pi);
axis_c_x = 90 - axis_c_x * (180.0/pi);
axis_c_y = 90 - axis_c_y * (180.0/pi);
axis_c_z = 90 - axis_c_z * (180.0/pi);

fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');

subplot(3,3,1)
var=axis_a_x;
histogram(var,100,'Normalization','pdf'); %var,numel(var)/40,'Normalization','pdf'
grid minor;
%pbaspect([1 1 1]);
title('major axis w.r.t yz plane');
xlabel("angle (deg)"+newline+" ");
ylabel('pdf');
xlim([-90, 90]);
xticks(-90:20:90);
ytickformat('%.3f');
%ylim([0, 0.015]);
set(gca,'TickDir','out');

subplot(3,3,2)
var=axis_a_y;
histogram(var,100,'Normalization','pdf');
grid minor;
%pbaspect([1 1 1]);
title('major axis w.r.t zx plane');
xlabel("angle (deg)"+newline+" ");
ylabel('pdf');
xlim([-90, 90]);
xticks(-90:20:90);
ytickformat('%.3f');
%ylim([0, 0.015]);
set(gca,'TickDir','out');

subplot(3,3,3)
var=axis_a_z;
histogram(var,100,'Normalization','pdf');
grid minor;
%pbaspect([1 1 1]);
title('major axis w.r.t xy plane');
xlabel("angle (deg)"+newline+" ");
ylabel('pdf');
xlim([-90, 90]);
xticks(-90:20:90);
ytickformat('%.3f');
%ylim([0, 0.015]);
set(gca,'TickDir','out');

subplot(3,3,4)
var=axis_b_x;
histogram(var,100,'Normalization','pdf');
grid minor;
%pbaspect([1 1 1]);
title('medium axis w.r.t yz plane');
xlabel("angle (deg)"+newline+" ");
ylabel('pdf');
xlim([-90, 90]);
xticks(-90:20:90);
ytickformat('%.3f');
%ylim([0, 0.015]);
set(gca,'TickDir','out');

subplot(3,3,5)
var=axis_b_y;
histogram(var,100,'Normalization','pdf');
grid minor;
%pbaspect([1 1 1]);
title('medium axis w.r.t zx plane');
xlabel("angle (deg)"+newline+" ");
ylabel('pdf');
xlim([-90, 90]);
xticks(-90:20:90);
ytickformat('%.3f');
%ylim([0, 0.015]);
set(gca,'TickDir','out');

subplot(3,3,6)
var=axis_b_z;
histogram(var,100,'Normalization','pdf');
grid minor;
%pbaspect([1 1 1]);
title('medium axis w.r.t xy plane');
xlabel("angle (deg)"+newline+" ");
ylabel('pdf');
xlim([-90, 90]);
xticks(-90:20:90);
ytickformat('%.3f');
%ylim([0, 0.015]);
set(gca,'TickDir','out');

subplot(3,3,7)
var=axis_c_x;
histogram(var,100,'Normalization','pdf');
grid minor;
%pbaspect([1 1 1]);
title('minor axis w.r.t yz plane');
xlabel("angle (deg)"+newline+" ");
ylabel('pdf');
xlim([-90, 90]);
xticks(-90:20:90);
ytickformat('%.3f');
%ylim([0, 0.015]);
set(gca,'TickDir','out');

subplot(3,3,8)
var=axis_c_y;
histogram(var,100,'Normalization','pdf');
grid minor;
%pbaspect([1 1 1]);
title('minor axis w.r.t zx plane');
xlabel("angle (deg)"+newline+" ");
ylabel('pdf');
xlim([-90, 90]);
xticks(-90:20:90);
ytickformat('%.3f');
%ylim([0, 0.015]);
set(gca,'TickDir','out');

subplot(3,3,9)
var=axis_c_z;
histogram(var,100,'Normalization','pdf');
grid minor;
%pbaspect([1 1 1]);
title('minor axis w.r.t xy plane');
xlabel("angle (deg)"+newline+" ");
ylabel('pdf');
xlim([-90, 90]);
xticks(-90:20:90);
ytickformat('%.3f');
%ylim([0, 0.015]);
set(gca,'TickDir','out');

set(findall(gcf, '-property', 'fontSize'), 'fontSize', 18, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
%saveas(fh, strcat(fileToRead1, '.png'), 'png');
options.Format = 'png';
hgexport(fh, strcat(fileToRead1, '_axis_degree.png'), options);
