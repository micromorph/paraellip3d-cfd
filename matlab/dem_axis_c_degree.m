function dem_axis_c_degree(fileToRead1)
%userpath('/home/yanb/matlab/')
screenWidth = 0.65; % not 0.5

fIn = fopen(fileToRead1);
row = fscanf(fIn, '%d', 1);
col = 29;
text = fscanf(fIn, '%s', col);
numeric = zeros(row, col);
for i = 1:row
	for j = 1:col
  numeric(i, j) = fscanf(fIn, '%e', 1);
 end
end
fclose(fIn);

% angle w.r.t axis, x denotes axis x
axis_a_x = numeric(:, 9);
axis_a_y = numeric(:, 10);
axis_a_z = numeric(:, 11);
axis_b_x = numeric(:, 12);
axis_b_y = numeric(:, 13);
axis_b_z = numeric(:, 14);
axis_c_x = numeric(:, 15);
axis_c_y = numeric(:, 16);
axis_c_z = numeric(:, 17);

% angle w.r.t plane, x denotes x plane (i.e., yz plane)
axis_a_x = 90 - axis_a_x * (180.0/pi);
axis_a_y = 90 - axis_a_y * (180.0/pi);
axis_a_z = 90 - axis_a_z * (180.0/pi);
axis_b_x = 90 - axis_b_x * (180.0/pi);
axis_b_y = 90 - axis_b_y * (180.0/pi);
axis_b_z = 90 - axis_b_z * (180.0/pi);
axis_c_x = 90 - axis_c_x * (180.0/pi);
axis_c_y = 90 - axis_c_y * (180.0/pi);
axis_c_z = 90 - axis_c_z * (180.0/pi);

fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');

var=axis_c_z;
histogram(var,100,'Normalization','pdf');
grid minor;
pbaspect([1 1 1]);
title('minor axis w.r.t xy plane');
xlabel('angle (deg)');
ylabel('pdf');
xlim([-90, 90]);
ylim([0, 0.01]);
xticks(-90:20:90);
ytickformat('%.3f');
%ylim([0, 0.012]);
set(gca,'TickDir','out');

set(findall(gcf, '-property', 'fontSize'), 'fontSize', 28, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
%saveas(fh, strcat(fileToRead1, '.png'), 'png');
options.Format = 'png';
hgexport(fh, strcat(fileToRead1, '_axis_c_degree.png'), options);
