function dem_triaxial_stress_strain_multiConfining(fileToRead1)
%userpath('/home/yanb/matlab/')
screenWidth = 1.0; % 0.5 works for remote and local dual screens; 1.0 works for remote and local single screen.

%read valid directories
dirall=dir('.');
dirs=dirall([dirall(:).isdir]);
dirs=dirs(~ismember({dirs.name},{'.','..'}));
n_confining=size(dirs,1);

%define arrays of unknown sizes
max_row = 100000;
sigma1=zeros(max_row, n_confining);
sigma3=zeros(max_row, n_confining);
poisson=zeros(max_row, n_confining);
epsilon1=zeros(max_row, n_confining);
rowNum=zeros(n_confining);

%for each confining pressure, read its data and store in arrays
for CF=1:n_confining
 filename=[dirs(CF).name '/' fileToRead1];       
 timeStep = 1; %5e-7;
 newData1 = importdata(filename);
 %create new variables in the caller workspace from those fields.
 for i = 1:size(newData1.colheaders, 2)
  assignin('caller', genvarname(newData1.colheaders{i}), newData1.data(:,i));
 end
 
step = evalin('caller', 'iteration');
traction_x1 = evalin('caller', 'traction_x1');
traction_x2 = evalin('caller', 'traction_x2');
traction_y1 = evalin('caller', 'traction_y1');
traction_y2 = evalin('caller', 'traction_y2');
traction_z1 = evalin('caller', 'traction_z1');
traction_z2 = evalin('caller', 'traction_z2');
mean_stress  = evalin('caller', 'mean_stress ');
bulk_volume = evalin('caller', 'bulk_volume');
density = evalin('caller', 'density');
epsilon_x = evalin('caller', 'epsilon_x');
epsilon_y = evalin('caller', 'epsilon_y');
epsilon_z = evalin('caller', 'epsilon_z');
epsilon_v = evalin('caller', 'epsilon_v');
void_ratio = evalin('caller', 'void_ratio');
porosity = evalin('caller', 'porosity');
avgNormal = evalin('caller', 'avgNormal');
avgShear = evalin('caller', 'avgShear');
avgPenetr = evalin('caller', 'avgPenetr');
transEnergy = evalin('caller', 'transEnergy');
rotatEnergy = evalin('caller', 'rotatEnergy');
kinetEnergy = evalin('caller', 'kinetEnergy');
accruedTime = step * timeStep;

rowNum(CF) = size(traction_x1,1);
for i = 1:rowNum(CF)
 epsilon1(i,CF) = epsilon_z(i);
 sigma1(i,CF) = (traction_z1(i) + traction_z2(i) ) / 2.0;
 sigma3(i,CF) = (traction_x1(i) + traction_x2(i) + traction_y1(i) + traction_y2(i))/4.0;
 poisson(i,CF) = abs( (epsilon_x(i) + epsilon_y(i))/2.0 ./ epsilon_z(i)); 
end

end
%end of reading all data and storing in arrays.

%start plotting
fh = figure('units', 'normalized', 'outerposition', [0 0 screenWidth 1]);
set(fh, 'visible', 'on');

subplot(1,2,1)
%forceH = plot(accruedTime, traction_z1, 'r--o', accruedTime, traction_z2, 'm--', accruedTime, traction_x1, 'b-x', accruedTime, traction_x2, 'k-p','LineWidth', 1); 
%forceH = plot(epsilon_z,  sigma1 - sigma3, 'b-o', 'LineWidth', 3); 
for CF=1:n_confining
 plot(epsilon1(1:rowNum(CF),CF), sigma1(1:rowNum(CF),CF) - sigma3(1:rowNum(CF),CF), '-o', 'LineWidth', 3);
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Stress vs strain');
xlabel('Axial strain');
ylabel('Deviatoric stress (Pa)');
legend(dirs.name, 'location', 'southeast');
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
xlim([0, 0.20]);

subplot(1,2,2)
%forceH = plot(accruedTime, traction_z1, 'r--o', accruedTime, traction_z2, 'm--', accruedTime, traction_x1, 'b-x', accruedTime, traction_x2, 'k-p','LineWidth', 1); 
%forceH = plot(epsilon_z, poisson, 'b-o', 'LineWidth', 3);
for CF=1:n_confining
 plot(epsilon1(1:rowNum(CF),CF), poisson(1:rowNum(CF),CF), '-o', 'LineWidth', 3);
 hold on; 
end
grid on;
grid minor;
pbaspect([1 1 1]);
title('Poisson''s ratio');
xlabel('Axial strain');
ylabel('Poisson''s ratio');
legend(dirs.name);
legend('boxoff');
%axis([0, 3.5e-4, ylim]);
xlim([0, 0.20]);
ylim([0.3, 1.0]);

set(findall(gcf, '-property', 'fontSize'), 'fontSize', 22, 'fontWeight', 'bold');
set(gcf, 'paperpositionmode', 'auto');
%saveas(fh, strcat(fileToRead1, '.png'), 'png');
options.Format = 'png';
hgexport(fh, strcat(fileToRead1, '_stress_poisson.png'), options);
hold off;


