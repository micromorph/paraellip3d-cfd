#!/bin/sh
#platform=soilblast
platform=micromorph

if [ $platform == "soilblast" ]; then 
 g++ -c Vec.cpp
 g++ -I/usr/local/eigen-3.3.4 -c Tetra.cpp
 g++ -c plot_tetra_contact.cpp -I/usr/local/eigen-3.3.4 -I/usr/local/qhull-2015.2/src -L/usr/local/qhull-2015.2/lib -lqhullcpp -lqhull_r
 g++ -o plot_tetra_contact plot_tetra_contact.o Vec.o Tetra.o -L/usr/local/qhull-2015.2/lib -lqhullcpp -lqhull_r

 #before adding solid angles:
 #g++ -o plot_tetra_contact plot_tetra_contact.cpp -I/usr/local/qhull-2015.2/src -L/usr/local/qhull-2015.2/lib -lqhullcpp -lqhull_r

 #to run the code:
 #export PATH=/usr/local/qhull-2015.2/bin:$PATH
 #export LD_LIBRARY_PATH=/usr/local/qhull-2015.2/lib:$LD_LIBRARY_PATH

elif [ $platform == "micromorph" ]; then
 g++ -c Vec.cpp
 g++ -I/usr/local/eigen-3.3.9 -c Tetra.cpp
 g++ -c plot_tetra_contact.cpp -I/usr/local/eigen-3.3.9 -I/usr/local/qhull-2020.2-gcc-11.2.1/src -L/usr/local/qhull-2020.2-gcc-11.2.1/lib -lqhullcpp -lqhull_r
 g++ -o plot_tetra_contact plot_tetra_contact.o Vec.o Tetra.o -L/usr/local/qhull-2020.2-gcc-11.2.1/lib -lqhullcpp -lqhull_r

 #to run the code:
 #export PATH=/usr/local/qhull-2020.2-gcc-11.2.1/bin:$PATH
 #export LD_LIBRARY_PATH=/usr/local/qhull-2020.2-gcc-11.2.1/lib:$LD_LIBRARY_PATH

fi
