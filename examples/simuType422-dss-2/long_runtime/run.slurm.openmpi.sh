#!/bin/sh

##SBATCH --mail-type=ALL
##SBATCH --mail-user=yuhsuan.lee@colorado.edu

#SBATCH -J type422_2
#SBATCH -N 1
#SBATCH --ntasks-per-node=27
#SBATCH -t 168:00:00
#SBATCH -p batch

## load modules
module load openmpi-4.1.1-gcc-11.2.0

# export additional environment variables
export LD_LIBRARY_PATH=/usr/local/boost-1.76.0-openmpi-4.1.1-gcc-11.2.0/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=/usr/local/qhull-2020.2-gcc-11.2.0/lib:$LD_LIBRARY_PATH

## cd to work dir
cd $SLURM_SUBMIT_DIR
srun --mpi=pmix -N1 -n27 ./paraEllip3d input.txt
