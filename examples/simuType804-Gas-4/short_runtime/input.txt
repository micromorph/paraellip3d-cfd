###################################################################
# Below are 25 generic parameters required by every simulation
###################################################################

#simulation type
#001 - proceed simulation with preset state
#002 - tune up mass percentage from number percentage
#003 - trim particles to a certain space domain
#004 - remove particles inside a spherical domain
#005 - calculate mass percentage from particle file
#101 - deposit spatially scattered particles into a rigid container
#102 - resume deposition using specified data file of particles and boundaries
#201 - isotropic 1, low confining pressure
#202 - isotropic 2, increasing confining pressure
#203 - isotropic 3, loading-unloading-reloading
#301 - odometer 1
#302 - odometer 2, loading-unloading-reloading
#401 - triaxial 1
#402 - triaxial 2, loading-unloading-reloading
#411 - plane strain 1, in x direction
#412 - plane strain 2, loading-unloading-reloading
#501 - true triaxial 1, create confining stress state
#502 - true triaxial 2, increase stress in one direction
#601 - expand particles volume inside a virtual cavity
#602 - resume simulation after expanding particles volume inside a virtual cavity
#701 - couple with supersonic air flow, bottom "left" part, R-H conditions
#702 - couple with supersonic air flow, bottom "left" part
#703 - couple with supersonic air flow, rectangular "left" part
#704 - couple with supersonic air flow, spherical "left" part
#705 - couple with supersonic air flow, rectangular "left" part, and a zone below
#801 - pure supersonic air flow, bottom "left" part, R-H conditions
#802 - pure supersonic air flow, bottom "left" part
#803 - pure supersonic air flow, rectangular "left" part
#804 - pure supersonic air flow, spherical "left" part
#805 - pure supersonic air flow, rectangular "left" part, and a zone below
simuType  804

#grids/processes in x, y, z directions for MPI parallelization, which
#is recommended for more than thousands of particles. By default these
#values are 1 for serial computing. They must satisfy equation:
#mpiProcX * mpiProcY * mpiProcZ = np, where np is the number of processes
#specified in mpirun command line.
mpiProcX  3
mpiProcY  3
mpiProcZ  4

#threads per process for OpenMP parallelization, which is recommended 
#for more than thousands of particles. By default this value is 1 for 
#single-thread computing. If it > 1, multiple-threads computing is 
#invoked.
ompThreads  1

#starting time step, must >= 1
startStep  1

#ending time step, must >= 1
endStep  10

#starting snapshot number, (endsStep - startStep +1) must be divided by (endSnap - startSnap + 1)
startSnap  1

#ending snapshot number
endSnap  1

#time accrued prior to computation
timeAccrued  0

#time step size
timeStep  5.0E-7

#coefficient of mass scaling
massScale  1

#coefficient of moment scaling
mntScale  1

#coefficient of gravity scaling
gravScale  0

#gravitational acceleration
gravAccel  9.8

#damping ratio of background damping of force
forceDamp  0

#damping ratio of background damping of moment
momentDamp  0

#damping ratio of inter-particle contact
contactDamp  0.55

#coefficient of inter-particle static friction
contactFric  0.5

#coefficient of particle-boundary static friction
boundaryFric  0.5

#coefficient of inter-particle cohesion
#5.0e+8; cohesion between particles (10kPa)
contactCohesion  0

#particle Young's modulus
#quartz sand E = 45GPa
young  4.5E+10

#particle Poisson's ratio
#quartz sand v = 0.25
poisson  0.25

#particle specific gravity
#quartz sand Gs = 2.65
specificG  2.65

#maximum relative overlap
maxRelaOverlap  1.0E-2

###################################################################
# Below are additional parameters for simulation type 804
###################################################################

# compute grid size
gridSize  2.500000e-03

# drag coefficient
Cd  0.9

# time integration schemes: 1-Euler, 2-RK2, 3-RK3
# default 1
RK  1

# Riemann solver
#  0: Roe solver
#  1: HLLC solver
#  2: HLLE solver
#  3: Roe + HLLC (for negative phenomenon)
#  4: Roe + HLLE (for negative phenomenon)
#  5: Roe + exact (for negative phenomenon)
# -1: exact solver
# -2: Lax-Friedrichs scheme
# -3: Lax-Wendroff scheme (two-step Richtmyer version)
solver  -1

# CourantFriedrichsLewy condition
CFL  0.5

# ratio of specific heat capacity of air
airGamma  1.4

# "right" density
rightDensity  1.225

# "right" pressure
rightPressure  1.01325E+5

# "right" velocity
rightVelocity  0

# "left" part types
# 1 - bottom "left" part, R-H conditions
# 2 - bottom "left" part
# 3 - rectangular "left" part
# 4 - spherical "left" part
# 5 - rectangular "left" part with a zone below
leftType  4

# fluid domain
x1F  -2.1E-1
y1F  -2.1E-1
z1F  -2.1E-1
x2F  2.1E-1
y2F  2.1E-1
z2F  2.1E-1

# boundary condition for x1, x2, y1, y2, z1, z2 directions respectively
# 0: non-reflecting
# 1: reflecting
x1Reflecting  0
x2Reflecting  0
y1Reflecting  0
y2Reflecting  0
z1Reflecting  0
z2Reflecting  0

#center and radius of "left" part
x0L  0.0
y0L  0.0
z0L  0.0
r0L  5.0E-3

# "left" density
leftDensity  2000

# "left" pressure
leftPressure  3e+9

# "left" velocity
leftVelocity  2000
