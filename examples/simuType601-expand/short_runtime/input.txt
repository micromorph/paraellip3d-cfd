###################################################################                                         
# Below are 25 generic parameters required by every simulation                                              
###################################################################                                         

#simulation type
#001 - proceed simulation with preset state
#002 - tune up mass percentage from number percentage
#003 - trim particles to a certain space domain
#004 - remove particles inside a spherical domain
#005 - calculate mass percentage from particle file
#101 - deposit spatially scattered particles into a rigid container
#102 - resume deposition using specified data file of particles and boundaries
#201 - isotropic 1, low confining pressure
#202 - isotropic 2, increasing confining pressure
#203 - isotropic 3, loading-unloading-reloading
#301 - odometer 1
#302 - odometer 2, loading-unloading-reloading
#401 - triaxial 1
#402 - triaxial 2, loading-unloading-reloading
#411 - plane strain 1, in x direction
#412 - plane strain 2, loading-unloading-reloading
#501 - true triaxial 1, create confining stress state
#502 - true triaxial 2, increase stress in one direction
#601 - expand particles volume inside a virtual cavity
#602 - resume simulation after expanding particles volume inside a virtual cavity
#701 - couple with supersonic air flow, bottom "left" part, R-H conditions
#702 - couple with supersonic air flow, bottom "left" part
#703 - couple with supersonic air flow, rectangular "left" part
#704 - couple with supersonic air flow, spherical "left" part
#705 - couple with supersonic air flow, rectangular "left" part, and a zone below
#801 - pure supersonic air flow, bottom "left" part, R-H conditions
#802 - pure supersonic air flow, bottom "left" part
#803 - pure supersonic air flow, rectangular "left" part
#804 - pure supersonic air flow, spherical "left" part
#805 - pure supersonic air flow, rectangular "left" part, and a zone below
simuType  601

#grids/processes in x, y, z directions for MPI parallelization, which
#is recommended for more than thousands of particles. By default these
#values are 1 for serial computing. They must satisfy equation:
#mpiProcX * mpiProcY * mpiProcZ = np, where np is the number of processes
#specified in mpirun command line.
mpiProcX  3
mpiProcY  3
mpiProcZ  4

#threads per process for OpenMP parallelization, which is recommended 
#for more than thousands of particles. By default this value is 1 for 
#single-thread computing. If it > 1, multiple-threads computing is 
#invoked.
ompThreads  1

#starting time step, must >= 1
startStep  1

#ending time step, must >= 1
endStep  10

#starting snapshot number, (endsStep - startStep +1) must be divided by (endSnap - startSnap + 1)
startSnap  1

#ending snapshot number
endSnap  1

#time accrued prior to computation
timeAccrued  0

#time step size
timeStep  5.0E-7

#coefficient of mass scaling
massScale  1.0

#coefficient of moment scaling
mntScale  1.0

#coefficient of gravity scaling
gravScale  1.0

#gravitational acceleration
gravAccel  9.8

#damping ratio of background damping of force
forceDamp  0

#damping ratio of background damping of moment
momentDamp  0

#damping ratio of inter-particle contact
contactDamp  0.85

#coefficient of inter-particle static friction
contactFric  0.5

#coefficient of particle-boundary static friction
boundaryFric  0.5

#coefficient of inter-particle cohesion
#5.0e+8; cohesion between particles (10kPa)
contactCohesion  0

#particle Young's modulus
#quartz sand E = 45GPa
young  2.9E+10

#particle Poisson's ratio
#quartz sand v = 0.25
poisson  0.25

#particle specific gravity
#quartz sand Gs = 2.65
specificG  2.65

#maximum relative overlap
maxRelaOverlap  2.5E-1

###################################################################
# Below are additional parameters for simulation type 601
###################################################################

# file name of boundary file
boundaryFile  deposit_boundary_ini

# file name of particle file
particleFile  trim_particle_ini

# initialize particles state (velocity, omga, force, moment) from data file or not?
toInitParticle  0

# particle boundary tracking and grid updating mode
#-1 - update with boundaries. All others update with particles.
# 0 - update in +Z
# 1 - update in +X, -X, +Y, -Y, +Z, but +X, -X, +Y, -Y are preset for limited explosion range
# 2 - update in +X, -X, +Y, -Y, +Z
# 3 - update in +X, -X, +Y, -Y, +Z, -Z
# default 0
gridUpdate  0

#cavity coordinates
#cavityMinX cavityMinY cavityMinZ cavityMaxX cavityMaxY cavityMaxZ
cavityMinX  0.016
cavityMinY  0.016
cavityMinZ  0.016
cavityMaxX  0.024
cavityMaxY  0.024
cavityMaxZ  0.024

#expandPercent
#percentage by which the particles inside virtual cavity expand in
#each dimension, ie, radius a, b and c
#note: modify maxRelaOverlap accordingly
expandPercent  0.25

# drag coefficient
Cd  0

# ambient fluid density
fluidDensity  1.225
