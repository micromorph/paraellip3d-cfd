#!/bin/bash
## for Slurm
#SBATCH -N 1
#SBATCH -J simuType101
#SBATCH -t 24:00:00
#SBATCH -p pbatch
#SBATCH --mail-type=ALL
##SBATCH --mail-user=beichuan.yan@colorado.edu
#SBATCH -A uco

## load modules
module swap intel gcc/4.9.3
module swap mvapich2 openmpi/4.0.0

## cd to work dir
cd $SLURM_SUBMIT_DIR

## export additional environment variables
export LOCAL_INSTALL=/g/g92/yan15/local
export LD_LIBRARY_PATH=$LOCAL_INSTALL/boost-1.75.0-openmpi-4.0.0-gcc-4.9.3/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LOCAL_INSTALL/qhull-2020.2-gcc-4.9.3/lib:$LD_LIBRARY_PATH

srun -N1 -n36 ./paraEllip3d input.txt
