#!/bin/bash
## for Slurm
#SBATCH -N 1
#SBATCH -J simu
#SBATCH -t 00:05:00
#SBATCH -p pbatch
#SBATCH --mail-type=ALL
##SBATCH --mail-user=beichuan.yan@colorado.edu
#SBATCH -A uco

## load modules
module swap mvapich2 impi/2019.8

## cd to work dir
cd $SLURM_SUBMIT_DIR

## export additional environment variables
export LOCAL_INSTALL=/g/g92/yan15/local
export LD_LIBRARY_PATH=$LOCAL_INSTALL/qhull-2020.2-intel-19.0.4/lib:$LD_LIBRARY_PATH
export LD_LIBRARY_PATH=$LOCAL_INSTALL/boost-1.75.0-impi-2019.8-intel-19.0.4/lib:$LD_LIBRARY_PATH

srun -N1 -n1 ./paraEllip3d input.txt
